# fb2-hjemmeside

Hjemmeside for fb2.no. Her lagres alle filene som er tilgjengelige på internettet. Det er ingen automatisk oppdatering av nettsiden, det må gjøres manuelt av et styremedlem.