Ordinært sameiemøte 20. april 2021
==================================

Den **20. april 2021** vil det ordinære årsmøtet holdes på Zoom. Møterommet åpnes klokken **18:00**, og selve møtet starter **18:30**. Dersom du ikke er vant med å bruke zoom, kan det være lurt å møte opp tidlig, i tilfelle du ikke kommer inn på første forsøk.

Det vil også være mulig å delta på møtet over telefon.

Alle detaljer om hvordan man kommer seg inn i møtet finnes i innkallingen (se under), og i epost som er sendt ut til alle sameierne.

Det er også mulig å bli med i møtet ved å klikke på [denne lenken](https://zoom.us/j/97726842709?pwd=a1liQ05xdEJWOUZLbzBZS01QdHhPdz09). Dette vil åpne møtet i appen, hvis du har installert denne. Vi anbefaler å installere app-en først, for den virker bedre en zoom i nettleseren. Du trenger ikke en konto for å bli med i møtet.

Dokumenter for nedlasting:

* [Innkalling med vedlegg (regnskap, budsjett og sakspapirer)](Innkalling_FB2_2021-04-20.pdf)
