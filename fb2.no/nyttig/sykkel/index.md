Sykkel
======

Sykkelparkering ute
-------------------

Sykler skal parkeres ved sykkelstativene bak blokken. Ikke parker sykler slik at man blokkerer for vaskeplassen.

Se [regler for fellesarealer](/nyttig/fellesarealer/).

Sykkelparkering inne
--------------------

Sameiet har en egen innendørs sykkelparkering. Se [vår parkeringsside](/nyttig/parkering/), for informasjon om leie.

Merking før høstdugnad
----------------------

Alle sykler som er parkert ute må merkes med navn og leilighetsnummer, før høstdugnaded. Sykler som ikke er merket vul bli fjernet av styret.

Vaskeplass for syler
--------------------

Sameiet har laget en vaskeplass for sykler, ved sykkelstativene på baksiden av blokken.


