Håndverkere
===========

Nedenfor er det listet opp håndverkere som kjenner sameiet godt. Vi oppfordrer beboere til å benytte disse håndverkerene.

Elektriker
----------

<table>
<tr><td class="a">Mythe’s Installasjon v/Oddbjørn Mythe</td><td class="b">909 43 166</td></tr>
</table>

Rørlegger
---------

<table>
<tr><td class="b">Torstein Solli AS</td><td class="a">480 00 100</td></tr>
</table>

