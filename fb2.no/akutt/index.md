Akutt
=====

Brann
-----

Brannvesenet nås på telefonnummer          110

For mer informasjon gå til <a href="/nyttig/brannvern/">denne siden</a>.

Politi
------

Politi nås på telefonnummer:               112

Ambulanse
---------

Ambulanse nås på telefonnummer:            113

Legevakt mm.
------------

<table>
	<tr>
		<td class="a">Legevakt:</td>
          <td class="b">22 93 22 93</td>
	</tr>
	<tr>
		<td class="b">Apotek1, legevakten (døgnåpent):</td>
		<td class="a">22 98 87 20</td>
	</tr>
	<tr>
		<td class="a">Tannlegevakten:</td>
          <td class="b">22 56 40 00 / 22 67 30 00</td>
	</tr>
</table>

## Heisstans
Dersom personer er innesperret i heisen bruk alarmknappen, eller ring NOKAS Heisalarm på telefon 02580.
Om heisen står og ingen er innesperret, vennligst [informér styret](/styret/).

## Avløp
Hver seksjonseier er ansvarlig for egne rør (dvs. alle rør i leiligheten). Ta kontakt med [vår rørlegger](/nyttig/handverkere/) for hjelp.

## Lekkasje
Er det store lekkasjer så skal brannvesenet ringes på telefon 110.
Har du behov for akutt hjelp, i forbindelse med mindre lekasje, kan [Rørteknikk VVS](https://rorteknikk.no/kontakt/) kontaktes.
De har døgnbemannet vakttelefon, og kan nås på telefon 23 33 80 50 ([prisliste](http://www.rorteknikk.no/prisliste.asp)).
Er ikke problemet akutt kan [våre egne tilknyttede rørleggere](nyttig/handverkere/) kontaktes.
I akutte tilfeller [informér styret](/styret/) så raskt som mulig.
Se også [denne siden for mer informasjon om lekkasjer](/nyttig/lekkasjer/).

## Vann- og Avløpsetaten
Er det problemer med drikkevann eller lignende, [kontakt Vann- og avløpsetaten](https://www.oslo.kommune.no/vann-og-avlop/).

## Bydel Nordre Aker
Kontaktinformasjon til vår bydel, Nordre Aker, [finner du her](https://www.oslo.kommune.no/bydeler/bydel-nordre-aker/).

## Annet
Se også vår [liste over tilknyttede håndverkere](/nyttig/handverkere/).
