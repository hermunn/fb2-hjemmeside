Hovedside
=========

Velkommen til Sameiet FB2
-------------------------

Velkommen til Sameiet Folke Bernadottes vei 2. Her kan du lese om sameiet, få praktisk informasjon og finne dokumenter som er relevante for beboere, eiere, håndtverkere og eiendomsmeglere.

[Den gamle siden](https://sites.google.com/a/fb2.no/sameiet/) er nå erstattet med denne.
