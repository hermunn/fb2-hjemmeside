Nyheter og meldinger fra styret
===============================

Angående oppussing i blokka og klager
-------------------------------------
*29. januar 2021*

Som noen har sett på oppslag i inngangsetasjen, er det varslet om støyende arbeider fra mandag, i fjerde etasje. Bråket dere har hørt i dag, kommer fra en annen leilighet.

Dette er ikke første gangen noe slikt skjer. Hvis dere klager på ikke varslet bråk, prøv å være 100% sikker på hvor det kommer fra, før dere klager.

Forøvrig, en klage til styret skal være skriftlig, som en lapp i postkassen med overskrift "Klage" eller en epost med tittel "Klage". Hvis du leverer en klage på støy som ikke har blitt varslet på forhånd, ta med tidsrom og hvem dere klager på.